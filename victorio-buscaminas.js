// elementos
const divTablero = document.getElementById('tablero');

// variables
const totalColumnas = 10;
const totalFilas = 10;
const totalBombas = 10;
const totalBanderas = 10;
const tablero = new Array(totalFilas);
var perdiste = false;
var ganaste = false;
var cantBanderas = 10;
var contSuccess = 0;

document.querySelector('#banderas').innerHTML = `Cantidad de banderas: ${cantBanderas}`;

function dibujarTablero() {

    for (let filas = 0; filas < totalFilas; filas++) {

        tablero[filas] = new Array(totalColumnas);

        for (let columna = 0; columna < totalColumnas; columna++) {
            const nuevaColumna = document.createElement('div');

            // attributos
            nuevaColumna.setAttribute('id', `f${filas}-c${columna}`);
            nuevaColumna.setAttribute('data-fila', filas);
            nuevaColumna.setAttribute('data-columna', columna);

            // clases
            nuevaColumna.classList.add("col-1", "m-2", "border", "text-center", "bg-light", "celda");

            // eventos


            nuevaColumna.addEventListener('click', function () {
                console.log('click columna');
                revelar(filas, columna);
            });



            nuevaColumna.addEventListener('contextmenu', function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (!nuevaColumna.classList.contains("bg-warning") && cantBanderas > 0) {
                    console.log('click derecho en columna', e);
                    nuevaColumna.classList.remove("bg-light");
                    nuevaColumna.classList.add("bg-warning")
                    cantBanderas--;
                    document.querySelector('#banderas').innerHTML = `Cantidad de banderas: ${cantBanderas}`;
                }
                else if (nuevaColumna.classList.contains("bg-warning")) {
                    nuevaColumna.classList.remove("bg-warning");
                    cantBanderas++;
                    document.querySelector('#banderas').innerHTML = `Cantidad de banderas: ${cantBanderas}`;
                }
            });

            tablero[filas][columna] = '1';

            divTablero.appendChild(nuevaColumna);
        }
    }

    // ubicamos las bombas
    ubicarBombas();

    // contamos bombas alrededor
    agregarTotalBombasAlTablero();

    console.log(tablero);
}

function ubicarBombas() {

    for (let bombas = 0; bombas < totalBombas; bombas++) {
        const filaBomba = Math.round(Math.random() * (totalFilas - 1));
        const columnaBomba = Math.round(Math.random() * (totalColumnas - 1));

        console.log(filaBomba);
        console.log(columnaBomba);

        tablero[filaBomba][columnaBomba] = 'B';
    }

    console.log(tablero);

}


function revelar(filaClickeada, columnaClickeada) {
    if (!ganaste) {
        if (!perdiste) {

            const elementoClickeado = document.getElementById(`f${filaClickeada}-c${columnaClickeada}`);

            console.log('filaClickeada', filaClickeada);
            console.log('columnaClickeada', columnaClickeada);

            if (filaClickeada > -1 && filaClickeada < totalFilas && columnaClickeada > -1 && columnaClickeada < totalColumnas) {
                // elemento no este revelado
                if (!elementoClickeado.classList.contains('revelada')) {

                    // elemento no tiene una bandera
                    if (!elementoClickeado.classList.contains('bandera')) {

                        // obtenemos elemento del tablero
                        const cantidadBombasAlrededor = tablero[filaClickeada][columnaClickeada];

                        // rellenamos con el valor
                        if (cantidadBombasAlrededor !== 0) {
                            elementoClickeado.innerHTML = cantidadBombasAlrededor;
                        }

                        // colocarle la clase de revelado
                        elementoClickeado.classList.add('revelada');

                        elementoClickeado.classList.remove('bg-light');
                        elementoClickeado.classList.add('bg-success');
                        contSuccess++;


                        // si tiene una bomba - fuiste
                        // si el valor es igual a nada 0
                        if (cantidadBombasAlrededor === 'B') {
                            elementoClickeado.classList.remove('bg-success');
                            elementoClickeado.classList.add('bg-danger');
                            alert('Perdiste sos malisimo!');
                            perdiste = true;

                        } else if (cantidadBombasAlrededor === 0) {
                            // volver a llamar a revelar

                            revelar(filaClickeada - 1, columnaClickeada - 1);
                            revelar(filaClickeada - 1, columnaClickeada);
                            revelar(filaClickeada - 1, columnaClickeada + 1);
                            revelar(filaClickeada, columnaClickeada - 1);
                            revelar(filaClickeada, columnaClickeada + 1);
                            revelar(filaClickeada + 1, columnaClickeada - 1);
                            revelar(filaClickeada + 1, columnaClickeada);
                            revelar(filaClickeada + 1, columnaClickeada + 1);
                        }
                    }
                }
            }
        }
        else {
            alert("No podes seguir jugando");
        }
        //contarCantidadCeldasVerdes();
        if (contSuccess === 90) {
            ganaste = true;
        }
        jugadorGano();
    }
    else {
        alert("Ya ganaste, empeza de nuevo");
    }
}

function agregarTotalBombasAlTablero() {
    for (let fila = 0; fila < totalFilas; fila++) {
        for (let columna = 0; columna < totalColumnas; columna++) {
            if (tablero[fila][columna] !== 'B') {
                tablero[fila][columna] = contarCantidadBombasAlrededor(fila, columna);
            }
        }
    }
}

function contarCantidadBombasAlrededor(fila, columna) {
    let cantidadBombas = 0;

    for (let posicionY = fila - 1; posicionY <= fila + 1; posicionY++) {
        for (let posicionX = columna - 1; posicionX <= columna + 1; posicionX++) {
            if (posicionY > -1 && posicionY < totalFilas && posicionX > -1 && posicionX < totalColumnas) {
                if (tablero[posicionY][posicionX] === 'B') {
                    cantidadBombas++;
                }
            }
        }
    }

    return cantidadBombas;
}

function contarCantidadCeldasVerdes() {
    contSuccess = 0;
    for (let posicionY = 0; posicionY <= 9; posicionY++) {
        for (let posicionX = 0; posicionX <= 9; posicionX++) {
            if (tablero[posicionY][posicionX].classList.contains("bg-success")) {
                contSuccess++;
            }
        }
    }
    if (contSuccess === 90) {
        ganaste = true;
    }
}

function jugadorGano() {
    if (ganaste) {
        alert("Ganaste!")
    }
}

dibujarTablero();